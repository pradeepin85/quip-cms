// Copyright 2017 Quip


import { getRootComponent } from "./root.jsx";
import { BaseMenu } from "../../shared/base-field-builder/base-menu.js";
import SingleTonAuth from "./SingleTonAuth";

export const DEFAULT_API_VERSION = "47.0";

export function getAuth() {
    var auth = new SingleTonAuth().auth;
    return auth;
}

let selectedFieldEntity;
let isLogoutInProgress_ = false;

export class FieldBuilderMenu extends BaseMenu {
    allMenuCommands() {
        return [

            {
                id: "refresh",
                label: quiptext("Refresh"),
                handler: () => this.refresh()

            },
            {
                id: "choose-cms",
                label: quiptext("Choose Content type"),
                handler: () => this.chooseCMS()
            },
            {
                id: "save",
                label: quiptext("Save"),
                handler: () => this.save()
            },
            {
                id: "openInSF",
                label: quiptext("OpenInSF"),
                handler: () => this.openInSf()
            }


        ];
    }

    login = () => {
        console.debug("login");
        //this.setState({ loadingLogin: true });
        return getAuth()
            .login()
            .then(() => {
                this.updateIsLoggedIn();
            })
            .finally(() => {
                this.updateIsLoggedIn();
            });
    };

    logout = () => {
        this.setState({ loadingLogin: true });
        return getAuth()
            .logout()
            .then(this.updateIsLoggedIn)
            .finally(() => {
                this.setState({ loadingLogin: false });
            });
    };

    updateIsLoggedIn = () => {
        console.log("dfsfsfdsfsd")
        this.setState({
            isLoggedIn: true,
        });
    };

    chooseCMS = () => {
        //this.login();
        getRootComponent().updateIsLoggedIn();
    };
    refresh = () => {
        var that = this;
        const record = quip.apps.getRootRecord();
        const mcvId = record.get("mcvId");
        const workspaceId = record.get("workspaceId");
        const url = record.get("host") + "/services/data/v47.0/connect/managed-content/content-spaces/" + workspaceId + "/content-versions/" + mcvId;
        var oReq = new XMLHttpRequest();
        oReq.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {

                record.refresh(JSON.parse(this.response));
                getRootComponent().refresh();
            }
        };
        oReq.open("GET", url);
        oReq.setRequestHeader('Authorization', 'Bearer ' + record.get('sessiontoken'));
        oReq.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        oReq.setRequestHeader("Access-Control-Allow-Origin", "*");
        oReq.send();
    };

    save = () => {
        const record = quip.apps.getRootRecord();
        const workspaceId = record.get("workspaceId");
        var fields = record.getFields();
        var data = {};

        var title;
        fields.map((field) => {
            var value;
            var b = fields[1].getDom().refs["rich-text-box"]
            if (field.get('type') === "RTE") {
                value = b.querySelector('div.content').innerHTML;
            } else if (field.get('type') === "NAMEFIELD") {
                value = field.getDom().refs['rich-text-box'].textContent;
                title = value;
            } else {
                value = field.getDom().refs['rich-text-box'].textContent;
            }
            var name = field.getName();
            data[name] = value;

        })
        data.image = {
            ref: "03Sxx0000004C92",
            url: "/file-asset/event51"
        };
        var mcID = record.get("mcId");
        var payload = this.createaPayload(mcID, record.get("contentType"), record.get("urlName"), data, title);
        this.saveContent(workspaceId, record.get("contentType"), record.get("mcvId"), payload);

    }

    saveContent = (workspaceId, contentype, mcvId, payload) => {
        const record = quip.apps.getRootRecord();
        var action = "CREATE_DRAFT";
        var that = this;
        var oReq = new XMLHttpRequest();
        oReq.onreadystatechange = function () {

            if (this.readyState == 4 && this.status == 201) {
                const record = quip.apps.getRootRecord();
                record.set("mcId", JSON.parse(this.response).managedContentId);
                record.set("mcvId", JSON.parse(this.response).id);
                record.set("urlName", JSON.parse(this.response).urlName);
            }
        };
        var method = "post";
        var url = record.get("host") + "/services/data/v47.0/connect/managed-content/content-spaces/" + workspaceId + "/content-versions";

        if (mcvId && mcvId !== "") {
            action = "SAVE_DRAFT";
            url = url + "/" + mcvId;
            method = "PATCH";
        }
        oReq.open(method, url + "?contentSpaceId=" + workspaceId + "&actionName=" + action + "&contentTypeDevName=" + contentype);
        oReq.setRequestHeader('Authorization', 'Bearer ' + record.get('sessiontoken'));
        oReq.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        oReq.setRequestHeader("Access-Control-Allow-Origin", "*");
        oReq.send(JSON.stringify(payload));

    }

    createaPayload = (managedContentId, contentype, urlName, data, title) => {
        const record = quip.apps.getRootRecord();

        if (data.hasOwnProperty(record.get("quipfiled"))) {
            data[record.get("quipfiled")] = "https://quip.com/" + quip.apps.getThreadId();
        }
        var payload = {
            "title": title,
            "body": data
        };
        if (managedContentId && managedContentId !== "") {
            payload.managedContentId = managedContentId;
            payload.urlName = urlName;
        } else {
            payload.type = contentype;
        }



        return payload;
    };

    openInSf = () => {
        const record = quip.apps.getRootRecord();
        var url = record.get("host") + "/lightning/cms/spaces/" + record.get('workspaceId') + "/content/" + record.get('mcId') + "/" + record.get('mcvId') + "?ws=%2Flightning%2Fcms%2Fspaces%2F" + record.get('workspaceId');
        Object.assign(document.createElement('a'), {
            target: '_blank',
            href: url,
        }).click();
    }
}