import Dialog from "../../shared/dialog/dialog.jsx";

import {
    Card,
    CardEmpty,
    CardFilter,
    DataTable,
    DataTableCell,
    DataTableColumn,
    Spinner
} from "@salesforce/design-system-react";


import { decode } from "he";
import moment from "moment";
import CMSContent from "./CMSContent.jsx";
import ButtonPrompt from "./ButtonPrompt.jsx";
import "./DashboardPicker.css";
import Styles from "./DashboardPicker.less";
import SingleTonAuth from "./SingleTonAuth";


const HEIGHT = 500;
const WIDTH = 500;

const ClickableDataTableCell = ({children, ...props}) => <DataTableCell
    {...props}>
    <div
        onClick={() => {
            props.onClick(props.item);
        }}
        style={{cursor: "pointer"}}>
        <a>{children}</a>
    </div>
</DataTableCell>;
ClickableDataTableCell.displayName = DataTableCell.displayName;

const LabelDataTableCell = ({children, ...props}) => <ClickableDataTableCell
    {...props}>
    <div
        onClick={() => {
            props.onClick(props.item);
        }}
        style={{cursor: "pointer"}}>
        <a>{props.item}</a>
    </div>
</ClickableDataTableCell>;
LabelDataTableCell.displayName = DataTableCell.displayName;

const MomentDataTableCell = ({children, ...props}) => <ClickableDataTableCell
    {...props}>
    <div
        onClick={() => {
            props.onClick(props.item);
        }}
        style={{cursor: "pointer"}}>
        <a>{decode(moment(props.item.lastModifiedDate).fromNow())}</a>
    </div>
</ClickableDataTableCell>;
MomentDataTableCell.displayName = DataTableCell.displayName;

const FolderDataTableCell = ({children, ...props}) => <ClickableDataTableCell
    {...props}
    width="150px"
    primaryColumn>
    <a>{decode(props.item.folder.label)}</a>
</ClickableDataTableCell>;
FolderDataTableCell.displayName = DataTableCell.displayName;


export function getAuth() {
    var auth =  new SingleTonAuth().auth;
    return auth;
}

export default class ApiTest extends React.Component {
    constructor(props) {
        super();
        this.state = {
            responseJson: null,
            showCommunityPicker:props.showCommunityPicker,
            showContentPicker:false,
            showContent:false,
            isFiltering:false,
            filterQuery: "",
            communities: [],
            totalCommunitiesLength: 0,
            allContent:[],
            selectedCommuninity:{},
            selectedContent:'',
            showLoader:false
        };
    }

    getContentTypeDefn = ()=>{
        const record = quip.apps.getRootRecord();

        var that = this;
        //HR request....
        function reqListener () {
          console.log(this.responseText);
          const responseJson = JSON.parse(this.responseText);
          const url=record.get("host") + "/services/data/v48.0/tooling/sobjects/ManagedContentType/0T1xx0000004CSOCA2";
          debugger;
          var contentNodes = responseJson.Metadata.managedContentNodeTypes;
              that.state.contentNodes = contentNodes;
              that.setState({ responseJson, responseEndpoint: url});
              
        }
        var oReq = new XMLHttpRequest();
        oReq.addEventListener("load", reqListener);
        oReq.open("GET", record.get("host") + "/services/data/v48.0/tooling/sobjects/ManagedContentType/"+record.get("contentTypeId"));
        oReq.setRequestHeader('Authorization','Bearer '+record.get('sessiontoken'));
        oReq.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        oReq.setRequestHeader("Access-Control-Allow-Origin", "*");
        oReq.send();
  }


    getAllCommunities = ()=>{
        const record = quip.apps.getRootRecord();


          var that = this;
          //HR request....
          function reqListener () {
            console.log(this.responseText);
            const responseJson = JSON.parse(this.responseText);
            //const responseJson = JSON.stringify(response.json(), null, 2);
                that.state.communities = responseJson["spaces"];
                that.state.totalCommunitiesLength = that.state.communities.length;
                that.setState({ responseJson, responseEndpoint: "cmspaces" });
          }
          var oReq = new XMLHttpRequest();
          oReq.addEventListener("load", reqListener);
          oReq.open("GET", record.get("host") + "/services/data/v48.0/connect/managed-content/content-spaces/authoring");
          oReq.setRequestHeader('Authorization','Bearer '+record.get('sessiontoken'));
          oReq.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
          oReq.setRequestHeader("Access-Control-Allow-Origin", "*");
          oReq.send();
    }
    
    getAllContent = (community)=>{
        var communityId = community.id;
        var contentType = 'news';
        this.state.showLoader = true;
        var _request = {
            url:'/services/data/v47.0/connect/communities/0DBB0000000TRpPOAW/managed-content/delivery?managedContentType=news',
            method: 'get',
            headers : {
                    "Content-Type" : "application/json"
                },
        };

        const { endpoint } = this.state;
        const tokenResponse = getAuth().getTokenResponse();
        const url = `${tokenResponse.instance_url}${_request.url}`;
        this.setState({ responseJson: `Loading ${url}`, responseEndpoint: null });
        getAuth()
            // Proxies the fetch through Quip's server to avoid CORS issues
            // and pass the bearer token automatically in the heads.
            .request({ url })
            .then(response => {
                this.state.showLoader = false;
                console.debug("Response from API", response);
                if (!response.ok) {
                    console.error("REQUEST ERROR", response);
                    return;
                }
                const responseJson = JSON.stringify(response.json(), null, 2);
                this.state.allContent = response.json()["items"];   
                this.state.showContentPicker = true;
        //this.setState({ totalCommunitiesLength: response.json().length});
                console.debug({ responseJson });
                this.setState({ responseJson, responseEndpoint: url });
            });
        console.log(_request);



    }

    componentDidMount() {

        this.getAllCommunities();
        
    }
    


    onSubmit = e => {


        showRecordPicker_ = e => {
            e.preventDefault();
            this.ensureLoggedIn().then(() => {
                this.props.menuDelegate.updateToolbar(
                    this.props.entity.getSelectedRecord());
                this.setState({showRecordPicker: true});
            });
        };

        const { endpoint } = this.state;
        const tokenResponse = getAuth().getTokenResponse();
        const url = `${tokenResponse.instance_url}${endpoint}`;
        this.setState({ responseJson: `Loading ${url}`, responseEndpoint: null });
        getAuth()
            // Proxies the fetch through Quip's server to avoid CORS issues
            // and pass the bearer token automatically in the heads.
            .request({ url })
            .then(response => {
                console.debug("Response from API", response);
                if (!response.ok) {
                    console.error("REQUEST ERROR", response);
                    return;
                }
                this.setState({showContentPicker});
                const responseJson = JSON.stringify(response.json(), null, 2);
                console.debug({ responseJson });
                this.setState({ responseJson, responseEndpoint: url });
            });
    };

    onClickRow = (community) => {
        console.debug("onClickRow", { community });
        //set current workspace id
        const record = quip.apps.getRootRecord();
        record.set("workspaceId", community.id);

        this.setState({showCommunityPicker: false});
        this.setState({community: community});
        this.setState({showContentPicker: false});
        this.setState({showContent:true});
        this.getContentTypeDefn(community);
    };

    onFilterChange = () =>{

    }

    onButtonPromptClick = () => {
            this.setState({showCommunityPicker: true});
    };


    communitiesDialog = (showCommunityPicker,communities,isEmpty,isFiltering, filterQuery,totalCommunitiesLength) => {
        
        let loadMessage;
        if (totalCommunitiesLength !== undefined) {
            loadMessage = `${totalCommunitiesLength} Communties found`;
        }
        return <Dialog onDismiss={() => this.setState({showCommunityPicker: false})}>
        <Card
            bodyClassName="slds-grow slds-scrollable--y "
            className="slds-grid slds-grid--vertical"
            heading={quiptext("Choose a Community")}
            empty={
                isEmpty && !isFiltering ? (
                    <CardEmpty heading="Loading ...">
                        <Spinner
                            assistiveText={{
                                label: quiptext("Loading"),
                            }}
                            size="large"/>
                    </CardEmpty>
                ) : null
            }
            style={{height: HEIGHT,width:WIDTH}}>
            <div className="slds-text-align_center slds-text-body_small slds-text-color_small">
                {loadMessage}
            </div>
            <DataTable items={communities}>
                <DataTableColumn label="Id" property="id">
                    <ClickableDataTableCell onClick={this.onClickRow}/>
                </DataTableColumn>
                <DataTableColumn label="Name" property="name">
                    <ClickableDataTableCell onClick={this.onClickRow}/>
                </DataTableColumn>
            </DataTable>
        </Card>
    </Dialog>;
    }

    allcontentDialog = (showContentPicker,allContent,isEmpty,isFiltering, filterQuery,totalCommunitiesLength) =>{
        
        let loadMessage;
        let contentLength = allContent.length;
        if (contentLength !== undefined) {
            loadMessage = `${contentLength} Content found`;
        }
        let newAllContent = allContent.map((item)=>{
            item.id = item.managedContentId;
            item.name = item.title;
            return item;
        })
        return <Dialog onDismiss={() => this.setState({showContentPicker: false})}>
        <Card bodyClassName="slds-grow slds-scrollable--y"
            className="slds-grid slds-grid--vertical"
            heading={quiptext("Choose a Content")}
            empty={
                isEmpty && !isFiltering ? (
                    <CardEmpty heading="Loading ...">
                        <Spinner
                            assistiveText={{
                                label: quiptext("Loading"),
                            }}
                            size="large"/>
                    </CardEmpty>
                ) : null
            }
            style={{height: HEIGHT,width:WIDTH}}>
            <div className="slds-text-align_center slds-text-body_small slds-text-color_small">
                {loadMessage}
            </div>
            <DataTable items={newAllContent}>
                <DataTableColumn label="Name" property="name">
                    <ClickableDataTableCell onClick={this.onClickContentRow}/>
                </DataTableColumn>
            </DataTable>
        </Card>
    </Dialog>;
    }

    showLoader = ()=>{
        return <Dialog onDismiss={() => this.setState({showLoader: false})}>
        <Card bodyClassName="slds-grow slds-scrollable--y"
            className="slds-grid slds-grid--vertical"
            empty={(<CardEmpty heading="Loading ...">
                        <Spinner
                            assistiveText={{
                                label: quiptext("Loading"),
                            }}
                            size="large"/>
                    </CardEmpty>) }
            style={{height: HEIGHT,width:WIDTH}}>
        </Card>
    </Dialog>;
    }

 decodeHTML =  (html)  => {
        var txt = document.createElement('textarea');
        txt.innerHTML = html;
        return txt.value;
    };

    showContent = (contentNodes) =>{
        
        
        
       
        if(contentNodes) {
            const record = quip.apps.getRootRecord();
            record.loadTemplate(contentNodes);
        }
        //const title = record.set("title", {  RichText_defaultText : content.title});
        //record.set("body", this.decodeHTML(content.contentNodes.body.value));
        //const imgUrl = record.set("imgUrl", c);
        //@JAGADEESH is going to do magic here
        return  <div></div>
        
    }

    render() {
        const {communities,showLoader,totalCommunitiesLength, contentNodes, isFiltering, filterQuery, showCommunityPicker,showContentPicker,allContent,selectedContent,showContent,community} = this.state;
        const isEmpty = communities.length === 0;
        let loadMessage;
        if (totalCommunitiesLength !== undefined) {
            loadMessage = `${totalCommunitiesLength} Communties found`;
        }

        return <div style={{width: "100%"}}>
        <div style={{height: "100%", width:"100%"}}>
            {showLoader ? (
                this.showLoader()
            ) : (<div></div>)}
            {showCommunityPicker ? (
                this.communitiesDialog(showCommunityPicker,communities,isEmpty,isFiltering, filterQuery,totalCommunitiesLength)
            ) : (
                <div>
               </div>
            )}
            {showContentPicker ? (
                this.allcontentDialog(showContentPicker,allContent,isEmpty,isFiltering, filterQuery,totalCommunitiesLength)
            ) : (<div></div>)}

            {showContent ? (
                this.showContent(contentNodes)
            ) : (<div></div>)}
            
            </div>
            <CMSContent/>  
        </div>
        ;


    }


    onClickContentRow = (content) => {
        
        console.debug("onClickRow", { content });
        this.setState({showContentPicker: false});
        this.setState({selectedContent: content});
        this.setState({showContent:true});


    };
}