import quip from "quip";

export default class CMSContent extends React.Component {
    imageNode = null;
    constructor(props) {
        super();
        this.state = {
            refresh: false,

        };
    }

    componentDidUpdate = () => {
        //const body = quip.apps.getRootRecord().get("body");
        //document.getElementById("body").innerHTML = body;
    }

    fieldRender = (items) => {
        let a = {};
        return <div >
            {items.map((obj, index) =>
                <div>
                    <div >

                        <quip.apps.ui.RichTextBox entity={obj.get("title")} ref={el => obj.setDom(el)} minHeight={obj.get("height")} width="100%" />

                    </div> <br /><br />

                </div>
            )}
        </div>;
    }

    refresh = () => {
        this.setState({
            refresh: true
        });
    }

    async setImage() {
        const record = quip.apps.getRootRecord();
        const imageRecord = record.get("image");
        const imageBlob = await getImage("https://storage.kraken.io/1O75XPG6WBhGrlzj6Q59/86d1e6573540d27ff715bcc44ca0638f/event51.jpg");
        imageRecord.uploadFile(imageBlob);

    }

    renderImage() {
        return
    }



    render() {

        const record = quip.apps.getRootRecord();
        let imageRecord;

        let fields = record.getFields();
        if (fields && fields.length) {
            fields = this.fieldRender(fields)
        }


        if (record.get("hasImage") === "Y") {
            imageRecord = record.get("image");
            this.setImage();
        }

        const isLoggedIn = this.state.isLoggedIn;
        let imgRenderCmp;

        if (record.get("hasImage") === "Y") {
            imgRenderCmp = <div onClick={this.imageClickHandler} style={{ width: "100%" }}>
                <quip.apps.ui.Image
                    allowResizing={false}
                    onWidthAndAspectRatioUpdate={() => { }}
                    ref={node => (this.imageNode = node)}
                    record={imageRecord}
                />
            </div>;
        } else {
            imgRenderCmp = <div></div>;
        }

        return <div style={{ width: "800px" }}>
            <br />

            <div style={{ borderColor: "gray", borderWidth: "1px", borderStyle: "solid", width: "100%" }} >
                {fields}
                {imgRenderCmp}
            </div>

            <br /><br />



        </div>

    }
}

function getImage(url) {
    return fetch(url)
        .then(response => {
            debugger;
            if (!response.ok) {
                throw Error(response.statusText);
            }
            return response.arrayBuffer();
        })
        .then(buffer => {
            debugger;
            return new Blob([new Uint8Array(buffer)]);
        });
}