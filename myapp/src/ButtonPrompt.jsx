import { Button } from "@salesforce/design-system-react";
import Styles from "./ButtonPrompt.less";

const ButtonPrompt = ({ onClick, text }) => <div
    className={Styles.loginContainer}
    onClick={onClick}>
    <Button onClick={onClick}>{text}</Button>
</div>;

export default ButtonPrompt;
