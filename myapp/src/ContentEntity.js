import quip from "quip";
import { idSuffixes } from "@salesforce/design-system-react/lib/components/card";

export class FieldEntity extends quip.apps.Record {

    static getProperties() {

        return {
            contentNodes: "string",
            key: "string",
            label: "string",
            name:"string",
            serverKey: "string",
            type: "string",
            title: quip.apps.RichTextRecord,
            originalValue: "object",
            height: "string",
            imgurl: "string"
        };;
    }
    
    notifyParent = () => {
        this.getParentRecord().notifyListeners();
    };
    getRawValue() {
        debugger;
        return this.get("title")
        .getTextContent()
        .trimRight();
    }
    getDom() {
        return this.node;
    }
    setDom(node) {
        return this.node = node;
    }

    getValue(){
        return this.get("title");
    }
    getName() {
        return this.get("name");
    }

    supportsComments() {
        return true;
    }
}

export class ContentEntity extends quip.apps.RootRecord {

    static getProperties() {
        return {
            host: "string",
            sessiontoken: "string",
            contentNodes: "string",
            workspaceId: "string",
            contentType: "string",
            contentTypeId: "string",
            mcId: "string",
            mcvId: "string",
            urlName: "string",
            isFirstTime : "string",
            fields: quip.apps.RecordList.Type(FieldEntity),
            image: quip.apps.ImageRecord,
            hasImage: "string",
            quipfiled: "string"    
        
        };
    }
    static getDefaultProperties() {
        return { fields: [], image: {} , isFirstTime : "Y", host: "https://jmaddila-ltm.internal.salesforce.com:6101",
        sessiontoken: "00Dxx0000006GPX!AQEAQBJDru_N0nZHZ9tObTdQdlF_DWlaV2J.BTJU_nmt1GQwXm0DEE4g6is81jOKfSr4CdlvNEqkjWwmAets1iJsn8og4V9R",
        contentType: "quiptype",
        contentTypeId: "0T1xx0000004CSOCA2",
        hasImage: "N",
        quipfiled : "quipurl"
    };
    }
    getFields() {
        return this.get("fields").getRecords();
    }

    getFields2() {
        return this.get("fields").getRecords();
    }

    getImage() {
        return this.get("image")
    }

    isFirstTime() {
        return this.get("isFirstTime")
    }
    

    reset() {
        this.fields = [];
    }

    getMCVId() {
        return this.get("mcvId");
    }

    loadPlaceHolderData() {

        if (this.get("fields") && this.get("fields").getRecords().length > 0) {
            return;
        }

        
        if (this.get("fields")) {
            this.get("fields").add({
                height: "20px",
                
                title: {
                    RichText_placeholderText: "Place holder for title",
                    RichText_defaultText:"Title"
                }

            });
        }
        if (this.get("fields")) {
            this.get("fields").add({
                height: "20px",
                title: {
                    RichText_placeholderText: "Place holder for body",
                    RichText_defaultText:"Body"
                }

            });
        }
    }
    refresh(dataToReload) {
       
        var contentNodes = JSON.parse(this.get("contentNodes"));
        if (!contentNodes && contentNodes.length === 0) {
            return;
        }
        this.clear("fields");
        var body = dataToReload.body;
        contentNodes.forEach((a) => {
            if (a.nodeType === 'NAMEFIELD' || a.nodeType === 'RTE' || a.nodeType === 'MTEXT'  || a.nodeType === "TEXT") {
               
                this.get("fields").add({
                    height: "20px",
                    name: a.nodeName,
                    type: a.nodeType,
                    title: {
                        RichText_defaultText: body[a.nodeName]
                    }
                });
            }
        });
        
    }

    loadTemplate(contentNodes) {
        debugger;
        this.set("contentNodes",JSON.stringify(contentNodes));
        if (!contentNodes && contentNodes.length === 0) {
            return;
        }
        //this.set("isFirstTime" , false);
        this.clear("fields");
        contentNodes.forEach((a) => {
            if (a.nodeType === 'NAMEFIELD' || a.nodeType === 'RTE' || a.nodeType === 'MTEXT' || a.nodeType === "TEXT") {
                var helpText = a.placeholderText;
                if(!a.helpText || 0 === a.helpText.length) {
                    helpText = a.nodeLabel
                }
                this.get("fields").add({
                    height: "20px",
                    name: a.nodeName,
                    type: a.nodeType,
                    title: {
                        RichText_placeholderText: helpText
                    }
                });
            } else if(a.nodeType ===  "IMG") {
                this.set("hasImage", "Y");

            }
        });
        this.set("isFirstTime","N");

    }

}
