let instance = null;

class SingleTonAuth{
    constructor() {
        if (!instance) {
            instance = this;
            var auth =  quip.apps.auth("salesforce");
            auth.login().then(()=>{
                //auth.request()
            });
            this._auth = auth;
        }
        return instance;
    }
  
    singletonMethod() {
      return 'singletonMethod';
    }
  
    static staticMethod() {
      return 'staticMethod';
    }
  
    get auth() {
      return this._auth;
    }
  
    set auth(value) {
      this._auth = value;
    }
  }
  
  export default SingleTonAuth;