import quip from "quip";
import App from "./App.jsx";
import { ContentEntity, FieldEntity } from "./ContentEntity.js";


import {FieldBuilderMenu} from "./menu.js";

class RootRecord extends quip.apps.RootRecord {
    static getProperties = () => ({
        title: quip.apps.RichTextRecord,
        body: "string",
        imgUrl: "string"
    })
    static getDefaultProperties = () => ({
        title: { RichText_placeholderText: "Add a title" },
        body : {RichText_defaultText :"Placeholder for Body"}
    })
    
    
}
quip.apps.registerClass(FieldEntity, "field");
quip.apps.registerClass(ContentEntity, "root");
//quip.apps.registerClass(RootRecord, "root");
const menuDelegate = new FieldBuilderMenu();
let rootComponent;

export function getRootComponent() {
    return rootComponent;
}
quip.apps.initialize({
    initializationCallback: function(rootNode, params) {
        const rootRecord = quip.apps.getRootRecord();
        if(rootRecord.isFirstTime() === "Y") {
            
            rootRecord.reset();
            rootRecord.loadPlaceHolderData();
        }

        ReactDOM.render(<App entity={rootRecord} ref={node => {
            rootComponent = node;
        }}/>, rootNode);
    },
    toolbarCommandIds: [
        "choose-cms",
        "save",
        "refresh",
        "openInSF"
    ],
    menuCommands: menuDelegate.allMenuCommands()
});
