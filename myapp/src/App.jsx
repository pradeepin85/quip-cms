import Styles from "./App.less";
import ApiTest from "./APITest.jsx";
import SingleTonAuth from "./SingleTonAuth";
import CMSContent from "./CMSContent.jsx";
import "@salesforce-ux/design-system/assets/styles/salesforce-lightning-design-system.min.css";

export const DEFAULT_API_VERSION = "47.0";

export function getAuth() {
    var auth =  new SingleTonAuth().auth;
    return auth;
}
let cmsContent;
export default class App extends React.Component {
    constructor(props) {
        super();
        this.state = {
            isLoggedIn: false,
            loadingLogin: false,        
        };
    }

    login = () => {
        console.debug("login");
        this.setState({ loadingLogin: true });
        return getAuth()
            .login()
            .then(()=>{
                this.updateIsLoggedIn();
            })
            .finally(() => {
                this.setState({ loadingLogin: false });
            });
    };

    logout = () => {
        this.setState({ loadingLogin: true });
        return getAuth()
            .logout()
            .then(this.updateIsLoggedIn)
            .finally(() => {
                this.setState({ loadingLogin: false });
            });
    };

    updateIsLoggedIn = () => {
        this.setState({
            isLoggedIn: true
        });
    };
    
    refresh = () => {
        cmsContent.refresh();
    };
    
l
    render() {
        const { isLoggedIn } = this.state;

        const text = !isLoggedIn ? "Log in" : "Log outA";
       
        return (
            
            <div>
                <div>
                {isLoggedIn && <ApiTest text={text} entity={this.props.entity} showCommunityPicker={true}/>}
                {!isLoggedIn && <CMSContent ref={node => {
                            cmsContent = node;
                        }}/>   }
                
                </div>
            </div>
        );
    }
} 